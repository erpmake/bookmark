var express = require('express');
var router = express.Router();

var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  fs.readdir('./txt', function(err, files){
    if (err) {
      console.log(err);
      return;
    }
    res.render('index', {value: files})
  });
});

router.get('/:name', function(req, res) {
  var name = req.params.name;
  fs.readFile("./txt/"+name, "UTF-8", function(err, data) {
    if (err) {console.error(err);}
    else {res.json(data)}
    })
})

module.exports = router;
