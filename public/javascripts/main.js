$('#catTree').jstree({
"core" : {
"animation" : 0,
"check_callback" :  function (op, node, par, pos, more) {
    if ((op === "move_node" || op === "copy_node") && node.type && node.type == "root") {
		return false;
	}
    if((op === "move_node" || op === "copy_node") && more && more.core && !confirm('確認移動?')) {
    return false;
  }
  return true;
},
},
"types" : {
  "#" : {
    "max_children" : 1,
    "max_depth" : 4,
    "valid_children" : ["root"]
  },
  "root" : {
    "icon" : "/static/3.3.9/assets/images/tree_icon.png",
    "valid_children" : ["default"]
  },
  "default" : {
    "valid_children" : ["default","file"]
  },
  "file" : {
    "icon" : "glyphicon glyphicon-file",
    "valid_children" : []
  }
},
"plugins" : [
  "contextmenu", "dnd", "search",
  "state", "types", "wholerow"
]
});
  
var rightFocus = -1;
function getData(index, name){
  rightFocus = index;
  $(".fileName").removeClass("active");
  $(".fileName").eq(index).addClass("active");

  $.ajax({
    url: "./"+name,
    async: false,   // asynchronous request? (synchronous requests are discouraged...)
    cache: false,   // with this, you can force the browser to not make cache of the retrieved data
    dataType: "text",  // jQuery will infer this, but you can set explicitly
    success: function(data) {
      let Ndata = data.substr(1, data.length-2);
      document.getElementById('bookMarkText').innerText = Ndata;
    }
  });
}

function dcreate() {
  var ref = $('#catTree').jstree(true);
  var sel = ref.get_selected();
  if(!sel.length) { return false; }
  sel = sel[0];
  sel = ref.create_node(sel, {"type":"file"});
  if(sel) {
    ref.edit(sel);
  }
};

function drename() {
  var ref = $('#catTree').jstree(true);
  var sel = ref.get_selected();
  if(!sel.length) { return false; }
  sel = sel[0];
  ref.edit(sel);
};

function ddelete() {
  var ref = $('#catTree').jstree(true);
  var sel = ref.get_selected();
  if(!sel.length) { return false; }
  else{
    let r = confirm('確定刪除?');
    if(r==true){
      alert('刪除成功');
      ref.delete_node(sel);
    }
  }

};

function expand(flag) {
  if(flag) {
    $("#left_con").css('flex', '0.4');
    $("#center_con").css('col-6', '0.4');
    $("#right_con").css('col-6', '0.2');

    $("#right_icon").css('display', 'none');
    $("#left_icon").css('display', '');
  }
  else {
    $("#left_con").css('flex', '0.2');
    $("#center_con").css('col-6', '0.6');
    $("#right_con").css('col-6', '0.2');

    $("#right_icon").css('display', '');
    $("#left_icon").css('display', 'none');
  }
}